//
//  ViewController.swift
//  AusPost
//
//  Created by Edward Jones on 6/10/2016.
//  Copyright © 2016 Edward Jones. All rights reserved.
//
//  AusPost demo app, shows earthquakes from a json feed on a mapkit with pulsing pins, pin color indicates magtitude of earthquake
//  Also shows the distance in meters to the nearest earthquake
//
//  I have used SVPulsingAnnotationView to show the earthquakes on a mapview in a fun way
//  https://github.com/TransitApp/SVPulsingAnnotationView

import UIKit
import MapKit

class SVAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var magtitude: Double
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, magtitude: Double) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.magtitude = magtitude
    }
}

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var earthquakesMapView: MKMapView!
    
    private let locationManager = CLLocationManager()
    private var earthquakes: [EarthquakeRecord]!
    private var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.earthquakesMapView.delegate = self
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //Swift compatible reachability library from
        //https://github.com/ashleymills/Reachability.swift/blob/master/ReachabilitySample/ViewController.swift
        let reachability = Reachability()!
        
        if reachability.isReachable {
            
            getEarthQuakes { (success, result) in
                
                if success {
                    
                    if let result = result {
                        self.earthquakes = result
                        self.refreshMap(data: result)
                        self.displayClosestEarthquake()
                    }
                    
                }
                
            }
            
        }else {
            
            let alertController = UIAlertController(title: "AusPost", message: "You're not connected to the internet, you will need to be connected to the internet to see any earhquakes", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                //No action required here
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (locations.count > 0) {
            locationManager.stopUpdatingLocation()
            currentLocation = locations.first!
            self.displayClosestEarthquake()
        }
        
    }
    
    //We don't know which will come back first, current GPS location or the list of earthquakes, this is in its own method so it can be called from both places
    private func displayClosestEarthquake() {
        
        if let currentLocation = self.currentLocation {
            
            let closestEarthQuake = closestEarthquake(currentLocation: currentLocation)
            
            if let closestEarthQuake = closestEarthQuake {
                
                let distance = CLLocation(latitude: Double(closestEarthQuake.lat)!, longitude: Double(closestEarthQuake.long)!).distance(from: currentLocation)
                
                let alertController = UIAlertController(title: "AusPost", message: "Nearest Earthquake to you is \(Int(distance)) meters", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //No action required here
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }
    
    }
    
    private func refreshMap( data : [EarthquakeRecord]) {
        
        for item in data {
            let location = CLLocation(latitude: Double(item.lat)!, longitude: Double(item.long)!)
            addPulsingAnnoation(location: location, magtitude: item.magnitude)
        }
        
    }
    
    func addPulsingAnnoation(location: CLLocation, magtitude: Double){
        
        let annotation = SVAnnotation.init(coordinate: location.coordinate, title: "", subtitle: "", magtitude: magtitude)
        self.earthquakesMapView.addAnnotation(annotation)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is SVAnnotation) {
                let identifier = "pulsingEarthquake"
                var pulsingView = (self.earthquakesMapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? SVPulsingAnnotationView)
                if pulsingView == nil {
                    pulsingView = SVPulsingAnnotationView.init(annotation: annotation, reuseIdentifier: identifier)
                    let svAnnotation = annotation as! SVAnnotation
                    
                    //Change the colour of the annocation based on the magtitude of the earthquake
                    if (svAnnotation.magtitude < 4.4) {
                        pulsingView!.annotationColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                    }else if (svAnnotation.magtitude < 5.0) {
                         pulsingView!.annotationColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                    }else {
                        pulsingView!.annotationColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                    }
                }
        
            return pulsingView
        }
        return nil
    }
    
    private func closestEarthquake(currentLocation : CLLocation) -> EarthquakeRecord! {
        
        var result: EarthquakeRecord!
        
        if let earthquakes = self.earthquakes {
            
            for earthquake in earthquakes {
                
                if result == nil {
                    result = earthquake
                }else {
                    
                    let location1 = CLLocation(latitude: Double(result.lat)!, longitude: Double(result.long)!)
                    let location2 = CLLocation(latitude: Double(earthquake.lat)!, longitude: Double(earthquake.long)!)
                    
                    if currentLocation.distance(from: location2) < currentLocation.distance(from: location1) {
                        result = earthquake
                    }
                    
                }
                
            }
            
        }
        
        return result
        
    }
    
    //ERJ - There are plenty of networking frameworks such as Alamofire which could be used to make this code a bit more compact and readable, however for this simple example URLSession does the job fine.
    private func getEarthQuakes(_ completion: @escaping (_ success : Bool, _ result: [EarthquakeRecord]?) -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
        URLSession.shared.dataTask(with: URL(string: "http://www.seismi.org/api/eqs/")!) {(data, response, error) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            var result = [EarthquakeRecord]()
            
            do {
                
                guard let data = data else {
                    //No data was returned
                    DispatchQueue.main.async {
                        completion(false, nil)
                    }
                    return
                }
            
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    //Something went wrong converting the json
                    DispatchQueue.main.async {
                        completion(false, nil)
                    }
                    return
                }
                
                let earthquakes = json["earthquakes"] as! NSArray
                
                for (item) in earthquakes {
                    
                    if let json = item as? NSDictionary {
                        result.append(EarthquakeRecord.init(json: json)!)
                    }
                    
                }
                
                DispatchQueue.main.async {
                    completion(true, result)
                }
                
            } catch {
                
                DispatchQueue.main.async {
                    completion(false, nil)
                }
                
            }
            
        }.resume()
        
    }


}

public struct EarthquakeRecord {
    
    public let lat: String
    public let long: String
    public let region: String
    public let magnitude: Double
    
    public init?(json: NSDictionary) {
        
        //ERJ - We could use something like Gloss to do the parsing for us but it would be overkil for this so just do it manually
        
        self.lat = json["lat"] as! String
        self.long = json["lon"] as! String
        self.region = json["region"] as! String
        self.magnitude = Double(json["magnitude"] as! String)!

    }
    
}

